/**
 * 存入localStorage
 */
export const setStore = (name, content) => {
  if (!name) return
  if (typeof content !== 'string') {
    content = JSON.stringify(content)
  }
  window.localStorage.setItem(name, content)
}

/**
 * 获取localStorage
 */
export const getStore = name => {
  if (!name) return
  return window.localStorage.getItem(name)
}

/**
 * 删除localStorage
 */
export const removeStore = name => {
  if (!name) return
  window.localStorage.removeItem(name)
}

/**
 * 设置cookie
 * @param {name} string 
 * @param {value} string 
 * @param {days} number 
 */
export const setCookie = (name, value, Days=7) => {
  var exp = new Date(); 
  exp.setTime(exp.getTime() + Days*24*60*60*1000); 
  document.cookie = name + "="+ escape (value) + ";expires=" + exp.toGMTString(); 
}

/**
 * 读取cookie
 * @param {name} string 
 */
export const getCookie = (name) =>{ 
  var arr,reg=new RegExp("(^| )"+name+"=([^;]*)(;|$)"); 
　return (arr=document.cookie.match(reg))?unescape(arr[2]):null;
}

/**
 * 删除cookie
 * @param {name} string 
 */
export const delCookie = (name) => { 
  var exp = new Date(); 
  exp.setTime(exp.getTime() - 1); 
  var cval=getCookie(name); 
  if(cval!=null) 
    document.cookie= name + "="+cval+";expires="+exp.toGMTString(); 
}

/**
 * 手机号的正则判断
 */
export const testPhoneNum = num => {
  let numReg = /^((13[0-9])|(14[5,7])|(15[0-3,5-9])|(17[0,3,5-8])|(18[0-9])|(199))\d{8}$/
  if (numReg.test(num)) {
    return true
  } else {
    return false
  }
}