const path = require('path')
const merge = require ('webpack-merge')
const config = require('./webpack.base.config')
const webpack = require('webpack')

const devConfig = {
	mode: "development",
	devtool: "cheap-module-eval-source-map",
	devServer: {
		host: "localhost",
		hot: true,
		hotOnly: true,
		open: true,
		port: 8080,
		historyApiFallback: true,
		contentBase: path.join(__dirname, '../dist'),
		proxy: {
			'/api': {
				target: 'http://192.168.0.106',
        changeOrigin: true,
        pathRewrite: {
          '^/api': '192.168.0.106:8080'
				}
			}
		}
	},
	resolve: {
		extensions: ['.js', '.jsx'],
		alias: {
			'@': path.join(__dirname, '../src/components'),
		}
	},
	plugins: [
		new webpack.HotModuleReplacementPlugin(),
		new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify('development')
    })
	]
}

module.exports = merge(config, devConfig)

